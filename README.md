# How To Compile Java App From Command-Line
## Pre-requisites
* Java JDK
* Add JAVA_HOME to environment variables:

![JAVA_HOME](images/JAVA_HOME.png "JAVA_HOME")

## Steps
1. java --version

![JavaVersion](images/JavaVersion.png "JavaVersion")

**NOTE** For more options, run
        
    java --help

2. javac --version

![JavacVersion](images/JavacVersion.png "JavacVersion")

**NOTE** For more options, run

    "javac --help"

3. jar --version

![JarVersion](images/JarVersion.png "JarVersion")

**NOTE** For more options, run

    jar --help

4. Write you App

5. Download your dependencies and put them inside /lib folder:

![LibFolder](images/LibFolder.png "LibFolder")

6. Add the /lib folder to your IDE (To fix your Java package import):

![AddDependencyLibFolder01](images/AddDependencyLibFolder01.png "AddDependencyLibFolder01")

![AddDependencyLibFolder02](images/AddDependencyLibFolder02.png "AddDependencyLibFolder02")

![AddDependencyLibFolder03](images/AddDependencyLibFolder03.png "AddDependencyLibFolder03")

![AddDependencyLibFolder04](images/AddDependencyLibFolder04.png "AddDependencyLibFolder04")

You should now see:

![AddDependencyLibFolder05](images/AddDependencyLibFolder05.png "AddDependencyLibFolder05")

source: https://stackoverflow.com/questions/1051640/correct-way-to-add-external-jars-lib-jar-to-an-intellij-idea-project/1051705#1051705

7. Compile your Java Main Class:

SYNTAX:

        javac --source-path src --class-path "<AbsolutePathToDependency1>;<AbsolutePathToDependency2>" -d <GeneratedClassesFolder> <PathToJavaMainClass>
EXAMPLE:

        javac --source-path src --class-path .\lib\gson-2.8.6.jar -d target\classes .\src\main\java\App.java

![CompileJavaWithDependency](images/CompileJavaWithDependency.png "CompileJavaWithDependency")

**NOTE** Make sure that the hierarchy matched:

![GeneratedClasses](images/GeneratedClasses.png "GeneratedClasses")

8. Create a MANIFEST file:

![CreateManifestFile](images/CreateManifestFile.png "CreateManifestFile")

9. Package your application into a JAR:

SYNTAX:

        jar --create --manifest .\MANIFEST.MF --file <GeneratedJarName> -C <GeneratedClassFolder> .

EXAMPLE:

        jar --create --manifest MANIFEST.MF --file target/jar/MyDemoApp.jar -C target/classes .

![CreateJarAndSetDestinationFolder](images/CreateJarAndSetDestinationFolder.png "CreateJarAndSetDestinationFolder")

10. Run your application:

SYNTAX:

      java --class-path <listOfDependentJarFiles> <MainClassAsDefinedInYourManifestFile>

EXAMPLE:

        java --class-path "lib\gson-2.8.6.jar;target/jar/MyDemoApp.jar" App

![RunningJarOkay](images/RunningJarOkay.png "RunningJarOkay")

Careful with "java.lang.ClassNotFoundException"

![java.lang.ClassNotFoundException](images/java.lang.ClassNotFoundException.png "java.lang.ClassNotFoundException")

OR

![RunningJarError](images/RunningJarError.png "RunningJarError")

## BEST Source
See: https://ant.apache.org/manual/tutorial-HelloWorldWithAnt.html
